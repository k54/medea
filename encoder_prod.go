package medea

import (
	"io"
	"sync"
)

// jsonEncoder is a JSON encoder, not safe for concurrent use.
// Use GetEncoder to have a safe and pooled instance
// Duplicate keys are NOT checked against
type jsonEncoder struct {
	buf []byte
}

func (je *jsonEncoder) replaceLast(b byte) {
	if len(je.buf) == 0 {
		je.writeByte(b)
		return
	}
	if je.buf[len(je.buf)-1] == ',' {
		je.buf[len(je.buf)-1] = b
	} else {
		je.writeByte(b)
	}
}
func (je *jsonEncoder) writeByte(b byte) {
	je.buf = append(je.buf, b)
}
func (je *jsonEncoder) writeString(s string) {
	je.buf = append(je.buf, s...)
}

func (je *jsonEncoder) Write(b []byte) (n int, err error) {
	n = len(b)
	je.buf = append(je.buf, b...)
	return
}

func (je *jsonEncoder) writeKey(key string) {
	if key == "" {
		return
	}
	je.writeString(`"`)
	je.writeString(key) // quite faster than writeEscapedString, and keys rarely have escape sequenes in them (error caught by debug encoder)
	je.writeString(`":`)
}

func (je *jsonEncoder) Cache() []byte {
	return je.buf
}

func (je *jsonEncoder) WriteCached(b []byte) {
	je.buf = append(je.buf, b...)
	return
}

func (je *jsonEncoder) WriteTo(w io.Writer) (n int64, err error) {
	if len(je.buf) == 0 {
		je.writeString("{}")
	} else {
		// remove last ','
		cur := len(je.buf) - 1
		for je.buf[cur] != ',' {
			cur--
		}
		copy(je.buf[cur:], je.buf[cur+1:])
		je.buf[len(je.buf)-1] = 0
		je.buf = je.buf[:len(je.buf)-1]
	}
	n, err = writeAll(je.buf, w)
	// reset buffer
	je.buf = je.buf[:0]
	return
}

func newEncoder() *jsonEncoder {
	return &jsonEncoder{
		buf: make([]byte, 0, smallBufferSize),
	}
}

var encPool = sync.Pool{
	New: func() interface{} {
		return newEncoder()
	},
}

func (je *jsonEncoder) Release() { encPool.Put(je) }
