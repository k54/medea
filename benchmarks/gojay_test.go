package benchmarks

import (
	"github.com/francoispqt/gojay"
)

func (ts *testStruct) MarshalJSONObject(enc *gojay.Encoder) {
	enc.StringKey("string", ts.String)
	enc.IntKey("int", ts.Int)
	// enc.FloatKey("float64", ts.Float64)
	enc.BoolKey("bool", ts.Bool)
	enc.SliceIntKey("ids", ts.Ids)
	enc.ArrayKey("items", gojay.EncodeArrayFunc(func(enc *gojay.Encoder) {
		for _, i := range ts.Items {
			enc.Object(i)
		}
	}))
}

func (ti *testItem) MarshalJSONObject(enc *gojay.Encoder) {
	enc.StringKey("name", ti.Name)
	enc.IntKey("id", ti.ID)
}
