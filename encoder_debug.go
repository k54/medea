package medea

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"strconv"
	"time"
)

// jsonEncoderDebug is a JSON encoder, not safe for concurrent use.
// Use GetEncoder to have a safe and pooled instance
// Duplicate keys are checked against (albeit poorly), but this encoder is way slower than the production one
type jsonEncoderDebug struct {
	buf      []byte
	keys     map[string]bool
	needkeys bool
	errors   []error
}

func (je *jsonEncoderDebug) replaceLast(b byte) {
	if len(je.buf) == 0 {
		je.writeByte(b)
		return
	}
	if je.buf[len(je.buf)-1] == ',' {
		je.buf[len(je.buf)-1] = b
	} else {
		je.writeByte(b)
	}
}
func (je *jsonEncoderDebug) writeByte(b byte) {
	je.buf = append(je.buf, b)
}
func (je *jsonEncoderDebug) startArray(key string) {
	je.Key(key)
	je.writeByte('[')
}
func (je *jsonEncoderDebug) endArray() {
	je.replaceLast(']')
	je.writeByte(',')
}
func (je *jsonEncoderDebug) writeString(s string) {
	je.buf = append(je.buf, s...)
}

func (je *jsonEncoderDebug) Key(key string) {
	if je.needkeys {
		if key == "" {
			je.errors = append(je.errors, fmt.Errorf("key omitted in an Object context"))
		}
	} else {
		if key != "" {
			je.errors = append(je.errors, fmt.Errorf(`key "%s" written in an Array context`, key))
		}
	}
	for _, c := range key {
		if c >= 0x20 && c != '"' && c != '\\' {
			continue
		}

		je.errors = append(je.errors, fmt.Errorf(`Invalid character in "%s": '%c'(0x%x)`, key, c, c))
	}
	if key == "" {
		return
	}
	// works poorly with nested objects
	// TODO: fix it
	if je.keys[key] {
		je.errors = append(je.errors, fmt.Errorf(`key "%s" already present`, key))
	}
	je.keys[key] = true
	je.writeByte('"')
	je.writeString(key)
	je.writeString(`":`)
}

func (je *jsonEncoderDebug) Cache() []byte {
	var b bytes.Buffer
	je.WriteTo(&b)
	return b.Bytes()
}
func (je *jsonEncoderDebug) WriteCached(b []byte) {
	je.buf = append(je.buf, b...)
}

func (je *jsonEncoderDebug) WriteTo(w io.Writer) (n int64, err error) {
	enc := getEncoder()
	enc.ObjectFunc("", func(_ Encoder) {
		enc.Key("data")
		enc.WriteCached(je.buf)
		enc.ArrayFunc("errors", func(enc Encoder) {
			for _, v := range je.errors {
				enc.Err("", v)
			}
		})
	})
	n, err = enc.WriteTo(w)
	enc.Release()
	return
}

func (je *jsonEncoderDebug) Release() {}

func (je *jsonEncoderDebug) Value(key string, value interface{}) {
	b, err := json.Marshal(value)
	if err != nil {
		je.Key(key)
		je.writeString(`"error: `)
		je.writeString(err.Error())
		je.writeString(`",`)
	} else {
		je.Key(key)
		je.WriteCached(b)
	}
}

func (je *jsonEncoderDebug) StringBytes(key string, value []byte) {
	je.Key(key)
	je.writeByte('"')
	je.buf = append(je.buf, value...)
	je.writeString(`",`)
}

func (je *jsonEncoderDebug) RawString(key, value string) {
	je.Key(key)
	je.writeByte('"')
	je.writeString(value)
	je.writeString(`",`)
}

func (je *jsonEncoderDebug) String(key, value string) {
	je.Key(key)
	je.writeByte('"')
	je.buf = writeEscapedString(je.buf, value)
	je.writeString(`",`)
}

func (je *jsonEncoderDebug) Err(key string, value error) {
	je.String(key, value.Error())
}

func (je *jsonEncoderDebug) Stringer(key string, value fmt.Stringer) {
	je.String(key, value.String())
}

func (je *jsonEncoderDebug) Float(key string, value float64) {
	je.Key(key)
	je.buf = strconv.AppendFloat(je.buf, value, 'f', -1, 64)
	je.writeByte(',')
}

func (je *jsonEncoderDebug) ArrayFunc(key string, value func(Encoder)) {
	octx := je.needkeys
	je.needkeys = false
	je.startArray(key)
	value(je)
	je.endArray()
	je.needkeys = octx
}

func (je *jsonEncoderDebug) Array(key string, value ArrayExporter) {
	if value.IsNil() {
		return
	}
	je.ArrayFunc(key, value.ExportArray)
}

func (je *jsonEncoderDebug) Nil(key string) {
	je.Key(key)
	je.writeString("null,")
}

func (je *jsonEncoderDebug) Bool(key string, value bool) {
	je.Key(key)
	if value {
		je.writeString("true,")
	} else {
		je.writeString("false,")
	}
}

func (je *jsonEncoderDebug) Object(key string, jo ObjectExporter) {
	if jo.IsNil() {
		return
	}
	je.ObjectFunc(key, jo.ExportObject)
}

func (je *jsonEncoderDebug) ObjectFunc(key string, value func(Encoder)) {
	je.Key(key)
	octx := je.needkeys
	je.needkeys = true
	je.writeByte('{')
	value(je)
	je.replaceLast('}')
	je.writeByte(',')
	je.needkeys = octx
}

func (je *jsonEncoderDebug) Int(key string, value int) {
	je.Int64(key, int64(value))
}

func (je *jsonEncoderDebug) Time(key string, value time.Time) {
	je.Int64(key, value.Unix())
}

func (je *jsonEncoderDebug) Int64(key string, value int64) {
	je.Key(key)
	je.buf = appendInt(je.buf, value)
	je.writeByte(',')
}

func (je *jsonEncoderDebug) Uint64(key string, value uint64) {
	je.Key(key)
	je.buf = appendUint(je.buf, value)
	je.writeByte(',')
}

func (je *jsonEncoderDebug) Bytes(key string, values []byte) {
	je.Key(key)
	base64.StdEncoding.EncodeToString(values)

	je.writeByte('"')
	enc := base64.StdEncoding
	buf := make([]byte, enc.EncodedLen(len(values)))
	enc.Encode(buf, values)
	je.buf = append(je.buf, buf...)
	je.writeString(`",`)
}

func (je *jsonEncoderDebug) IntSlice(key string, values []int) {
	je.startArray(key)
	for _, v := range values {
		je.Int("", v)
	}
	je.endArray()
}

func (je *jsonEncoderDebug) BoolSlice(key string, values []bool) {
	je.startArray(key)
	for _, v := range values {
		je.Bool("", v)
	}
	je.endArray()
}

func (je *jsonEncoderDebug) Int64Slice(key string, values []int64) {
	je.startArray(key)
	for _, v := range values {
		je.Int64("", v)
	}
	je.endArray()
}

func (je *jsonEncoderDebug) Uint64Slice(key string, values []uint64) {
	je.startArray(key)
	for _, v := range values {
		je.Uint64("", v)
	}
	je.endArray()
}

func (je *jsonEncoderDebug) FloatSlice(key string, values []float64) {
	je.startArray(key)
	for _, v := range values {
		je.Float("", v)
	}
	je.endArray()
}

func (je *jsonEncoderDebug) StringSlice(key string, values []string) {
	je.startArray(key)
	for _, v := range values {
		je.String("", v)
	}
	je.endArray()
}
