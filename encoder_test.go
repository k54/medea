package medea

import (
	"bytes"
	"io/ioutil"
	"math/rand"
	"os"
	"testing"
	"time"
)

func TestEncoderDebug(t *testing.T) {
	var w bytes.Buffer
	enc := GetEncoderDebug()
	enc.ObjectFunc("", func(enc Encoder) {
		enc.String("hello", "world")
		enc.String("hello", "there")
		enc.String(`general kenobi
`, `"testing"`)
	})
	_, err := enc.WriteTo(&w)
	enc.Release()
	t.Log(err, w.String())
}

// func TestMedeaEncoder(t *testing.T) {
// 	var w bytes.Buffer
// 	var bools []bool
// 	for i := 0; i < 4000; i++ {
// 		bools = append(bools, i%2 == 0)
// 	}
// 	var ints []int
// 	for i := 0; i < 4000; i++ {
// 		ints = append(ints, i)
// 	}
// 	enc := newMedeaEncoder()
// 	// enc.buf = enc.buf[:0]
// 	enc.IntSlice("", ints)
// 	enc.String("", "test")
// 	enc.Bool("", false)
// 	enc.Float("", 3.14)
// 	enc.Int64("", 42)
// 	enc.BoolSlice("", bools)
// 	_, err := enc.WriteTo(&w)
// 	enc.Release()
// 	t.Logf("err: %v", err)
// 	decode(w.Bytes())
// }
func BenchmarkWriteManual(b *testing.B) {
	enc := encPool.Get().(*jsonEncoder)
	enc.buf = enc.buf[:0]
	var bools []bool
	for i := 0; i < 4000; i++ {
		bools = append(bools, i%2 == 0)
	}
	for i := 0; i < b.N; i++ {
		enc.BoolSlice("test", bools)
		enc.WriteTo(ioutil.Discard)
	}
	enc.Release()
}
func BenchmarkWriteTypeSwitch(b *testing.B) {
	enc := encPool.Get().(*jsonEncoder)
	enc.buf = enc.buf[:0]
	var bools []bool
	for i := 0; i < 4000; i++ {
		bools = append(bools, i%2 == 0)
	}
	for i := 0; i < b.N; i++ {
		enc.Value("test", bools)
		enc.WriteTo(ioutil.Discard)
	}
	enc.Release()
}

func TestJsonFile(t *testing.T) {
	var bools []bool
	for i := 0; i < 4000; i++ {
		bools = append(bools, i%2 == 0)
	}
	enc := newEncoder()
	enc.ArrayFunc("", func(Encoder) {
		for i := 0; i < 20*60; i++ {
			enc.ArrayFunc("", func(Encoder) {
				enc.Int64("", time.Now().UnixNano())
				enc.Int64("", int64(i%3))
				enc.ArrayFunc("", func(Encoder) {
					for j := 0; j < 1220; j++ {
						enc.Int64Slice("", []int64{
							int64(rand.Float64() * 1000000),
							int64(rand.Float64() * 1000000),
							int64(rand.Float64() * 1000000),
						})
					}
				})
			})
		}
	})

	f, err := os.Create("data.json")
	if err != nil {
		panic(err)
	}
	// log.Printf("%s", enc.buf)
	_, err = enc.WriteTo(f)
	if err != nil {
		panic(err)
	}
	enc.Release()
	f.Sync()
}

/*
func TestMedeaFile(t *testing.T) {
	var bools []bool
	for i := 0; i < 4000; i++ {
		bools = append(bools, i%2 == 0)
	}
	enc := newMedeaEncoder()
	enc.ArrayFunc("", func(Encoder) {
		for i := 0; i < 20*60; i++ {
			enc.ArrayFunc("", func(Encoder) {
				enc.Int64("", time.Now().UnixNano())
				enc.Int64("", int64(i%3))
				enc.ArrayFunc("", func(Encoder) {
					for j := 0; j < 1220; j++ {
						enc.Int64Slice("", []int64{
							int64(rand.Float64() * 1000000),
							int64(rand.Float64() * 1000000),
							int64(rand.Float64() * 1000000),
						})
					}
				})
			})
		}
	})

	f, err := os.Create("data.medea")
	if err != nil {
		panic(err)
	}
	_, err = enc.WriteTo(f)
	if err != nil {
		panic(err)
	}
	enc.Release()
	f.Sync()
}
*/
