package benchmarks

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"io/ioutil"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/francoispqt/gojay"
	"gitlab.com/k54/medea"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestEncodeSmallManual(t *testing.T) {
	ts := getTS()
	buf := bytes.Buffer{}
	medea.MarshalTo(&buf, ts)

	t.Logf("%d\n", buf.Len())
}
func TestEncodeSmallStdEncodingJson(t *testing.T) {
	ts := getTS()
	buf := bytes.Buffer{}
	enc := json.NewEncoder(&buf)
	enc.Encode(ts)

	t.Logf("%d\n", buf.Len())
}
func TestEncodeSmallGob(t *testing.T) {
	ts := getTS()
	buf := bytes.Buffer{}
	enc := gob.NewEncoder(&buf)
	enc.Encode(ts)

	t.Logf("%d\n", buf.Len())
}
func BenchmarkEncodeSmallManualPooled(b *testing.B) {
	b.ReportAllocs()

	b.RunParallel(func(pb *testing.PB) {
		ts := getTS()
		for pb.Next() {
			medea.MarshalTo(ioutil.Discard, ts)
		}
	})
}
func TestEncodeStdEncodingJson(t *testing.T) {
	ts := getTS()

	val, _ := json.Marshal(ts)
	t.Logf("%s\n", val)
}

func BenchmarkEncodeSmallStdEncodingJson(b *testing.B) {
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		ts := getTS()
		for pb.Next() {
			_, _ = json.Marshal(ts)
		}
	})
}

func BenchmarkEncodeSmallGob(b *testing.B) {
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		ts := getTS()
		enc := gob.NewEncoder(ioutil.Discard)
		for pb.Next() {
			enc.Encode(ts)
			// medea.MarshalTo(ioutil.Discard, cols)
		}
	})
}

func TestEncodeGojay(t *testing.T) {
	enc := gojay.NewEncoder(ioutil.Discard)
	ts := getTS()

	sb := strings.Builder{}
	enc = gojay.NewEncoder(&sb)
	_ = enc.Encode(ts)
	t.Logf("%s\n", sb.String())
}
func BenchmarkEncodeGojay(b *testing.B) {
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		ts := getTS()
		enc := gojay.NewEncoder(ioutil.Discard)
		for pb.Next() {
			_ = enc.Encode(ts)
		}
	})

}

func BenchmarkEncodeBigStdEncodingJson(b *testing.B) {
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		cols := getColors()
		for pb.Next() {
			_, _ = json.Marshal(cols)
		}
	})
}

func TestEncodeBigManual(t *testing.T) {
	cols := getColors()
	buf := bytes.Buffer{}
	medea.MarshalTo(&buf, cols)

	t.Logf("%d\n", buf.Len())
}
func BenchmarkEncodeBigManual(b *testing.B) {
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		cols := getColors()
		for pb.Next() {
			medea.MarshalTo(ioutil.Discard, cols)
		}
	})
}

func BenchmarkEncodeBigGob(b *testing.B) {
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		cols := getColors()
		enc := gob.NewEncoder(ioutil.Discard)
		for pb.Next() {
			enc.Encode(cols)
			// medea.MarshalTo(ioutil.Discard, cols)
		}
	})
}

func BenchmarkAppendInt(b *testing.B) {
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			enc := medea.GetEncoder()
			for i := int64(-20000); i < 20000; i++ {
				enc.Int64("", i)
			}
			enc.Release()
		}
	})
}
func BenchmarkAppendUint(b *testing.B) {
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			enc := medea.GetEncoder()
			for i := uint64(0); i < 40000; i++ {
				enc.Uint64("", i)
			}
			enc.Release()
		}
	})
}
func BenchmarkAppendFloat(b *testing.B) {
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			enc := medea.GetEncoder()
			for i := float64(-20000); i < 20000; i++ {
				enc.Float("", i)
			}
			enc.Release()
		}
	})
}

func BenchmarkAppendBool(b *testing.B) {
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			enc := medea.GetEncoder()
			for i := 0; i < 20000; i++ {
				enc.Bool("", true)
				enc.Bool("", false)
			}
			enc.Release()
		}
	})
}

func BenchmarkAppendStringRaw(b *testing.B) {
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			enc := medea.GetEncoder()
			for i := 0; i < 40000; i++ {
				enc.RawString("test", "Hello world from ⚙️\n")
			}
			enc.Release()
		}
	})
}
func BenchmarkAppendString(b *testing.B) {
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			enc := medea.GetEncoder()
			for i := 0; i < 40000; i++ {
				enc.String("test", "Hello world from ⚙️\n")
			}
			enc.Release()
		}
	})
}

func BenchmarkAppendDurationStringer(b *testing.B) {
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		d := 1*time.Hour + 2*time.Minute + 300*time.Millisecond
		for pb.Next() {
			enc := medea.GetEncoder()
			for i := 0; i < 40000; i++ {
				enc.String("", d.String())
			}
			enc.Release()
		}
	})
}

func BenchmarkAppendDurationValue(b *testing.B) {
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		d := 1*time.Hour + 2*time.Minute + 300*time.Millisecond
		for pb.Next() {
			enc := medea.GetEncoder()
			for i := 0; i < 40000; i++ {
				enc.Value("", d)
			}
			enc.Release()
		}
	})
}

type tester struct {
	Valid  bool `json:"valid"`
	Number int  `json:"number"`
}

func BenchmarkAppendTesterObject(b *testing.B) {
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		t := tester{true, 42}
		for pb.Next() {
			enc := medea.GetEncoder()
			for i := 0; i < 40000; i++ {
				enc.ObjectFunc("", func(enc medea.Encoder) {
					enc.Bool("valid", t.Valid)
					enc.Int("number", t.Number)
				})
			}
			enc.Release()
		}
	})
}
func BenchmarkAppendTesterValue(b *testing.B) {
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		t := tester{true, 42}
		for pb.Next() {
			enc := medea.GetEncoder()
			for i := 0; i < 40000; i++ {
				enc.Value("", t)
			}
			enc.Release()
		}
	})
}

// func BenchmarkAppendStringEscaped(b *testing.B) {
// 	b.ReportAllocs()
// 	b.RunParallel(func(pb *testing.PB) {
// 		for pb.Next() {
// 			enc := medea.GetEncoder()
// 			for i := 0; i < 40000; i++ {
// 				enc.StringEscaped("test", "Hello world from ⚙️\n")
// 			}
// 			enc.Release()
// 		}
// 	})
// }

func BenchmarkAppendNull(b *testing.B) {
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			enc := medea.GetEncoder()
			for i := 0; i < 40000; i++ {
				enc.Nil("")
			}
			enc.Release()
		}
	})
}
func BenchmarkAppendIntSlice(b *testing.B) {
	var slice []int64
	for i := -20000; i < 20000; i++ {
		slice = append(slice, int64(i))
	}
	b.ResetTimer()
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			enc := medea.GetEncoder()
			enc.Int64Slice("", slice)
			enc.Release()
		}
	})
}
func BenchmarkAppendFloatSlice(b *testing.B) {
	var slice []float64
	for i := -20000; i < 20000; i++ {
		slice = append(slice, float64(i))
	}
	b.ResetTimer()
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			enc := medea.GetEncoder()
			enc.FloatSlice("", slice)
			enc.Release()
		}
	})
}
