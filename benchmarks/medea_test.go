package benchmarks

import (
	"fmt"

	"gitlab.com/k54/medea"
)

type jsonExample struct {
	Colors []*jsonColor `json:"colors"`
}

func (je *jsonExample) IsNil() bool { return je == nil }
func (je *jsonExample) ExportObject(enc medea.Encoder) {
	enc.ArrayFunc("colors", func(enc medea.Encoder) {
		for _, v := range je.Colors {
			enc.Object("", v)
		}
	})
}
func (jc *jsonColor) IsNil() bool { return jc == nil }

func (jc *jsonColor) ExportObject(enc medea.Encoder) {
	enc.String("color", jc.Color)
	enc.String("category", jc.Category)
	enc.String("type", jc.Type)
	enc.Object("code", jc.Code)
}
func (jcc *jsonColorCode) IsNil() bool { return jcc == nil }
func (jcc *jsonColorCode) ExportObject(enc medea.Encoder) {
	enc.IntSlice("rgba", jcc.RGBA)
	enc.String("hex", jcc.Hex)
}

type jsonColor struct {
	Color    string         `json:"color"`
	Category string         `json:"category"`
	Type     string         `json:"type"`
	Code     *jsonColorCode `json:"code"`
}

type jsonColorCode struct {
	RGBA []int  `json:"rgba"`
	Hex  string `json:"hex"`
}

var colorBlack = &jsonColor{Color: "black", Category: "hue", Type: "primary",
	Code: &jsonColorCode{RGBA: []int{255, 255, 255, 1}, Hex: "#000"},
}
var colorWhite = &jsonColor{Color: "white", Category: "value",
	Code: &jsonColorCode{RGBA: []int{0, 0, 0, 1}, Hex: "#FFF"},
}
var colorRed = &jsonColor{Color: "red", Category: "hue", Type: "primary",
	Code: &jsonColorCode{RGBA: []int{255, 0, 0, 1}, Hex: "#FF0"},
}
var colorBlue = &jsonColor{Color: "blue", Category: "hue", Type: "primary",
	Code: &jsonColorCode{RGBA: []int{0, 0, 255, 1}, Hex: "#00F"},
}
var colorYellow = &jsonColor{Color: "yellow", Category: "hue", Type: "primary",
	Code: &jsonColorCode{RGBA: []int{255, 255, 0, 1}, Hex: "#FF0"},
}
var colorGreen = &jsonColor{Color: "green", Category: "hue", Type: "secondary",
	Code: &jsonColorCode{RGBA: []int{0, 255, 0, 1}, Hex: "#0F0"},
}

func getColors() *jsonExample {
	var jc = []*jsonColor{}
	for i := 0; i < 40000; i++ {
		jc = append(jc, colorBlack, colorWhite, colorRed, colorGreen, colorBlue, colorYellow)
	}
	return &jsonExample{
		Colors: jc,
	}
}

type testStruct struct {
	// Float64 float64     `json:"float64"`
	Int    int         `json:"int"`
	String string      `json:"string"`
	Bool   bool        `json:"bool"`
	Ids    []int       `json:"ids"`
	Items  []*testItem `json:"items"`
}

func (ts *testStruct) reset() {
	ts.Int = 0
	ts.String = ts.String[:0]
	ts.Bool = false
	ts.Ids = ts.Ids[:0]
	ts.Items = ts.Items[:0]
}

type testItem struct {
	Name string
	ID   int
}

func (ti *testItem) String() string {
	return fmt.Sprintf(`{Id:%d Name:%s}`, ti.ID, ti.Name)
}

func (ts *testStruct) ExportObject(enc medea.Encoder) {
	enc.String("string", ts.String)
	enc.Int("int", ts.Int)
	// enc.Float("float64", ts.Float64)
	enc.Bool("bool", ts.Bool)
	enc.IntSlice("ids", ts.Ids)
	enc.ArrayFunc("items", func(enc medea.Encoder) {
		for _, ti := range ts.Items {
			enc.ObjectFunc("", func(enc medea.Encoder) {
				enc.String("name", ti.Name)
				enc.Int("id", ti.ID)
			})
		}
	})
}

func (ts *testStruct) IsNil() bool {
	return ts == nil
}
func (ti *testItem) IsNil() bool {
	return ti == nil
}

func getTS() *testStruct {
	return &testStruct{
		Int:    42,
		String: "the answer to life, the universe, and everything else",
		Bool:   true,
		// Float64: 3.1415,
		Items: []*testItem{
			{Name: "one", ID: 1},
			{Name: "two", ID: 2},
			{Name: "two", ID: 2},
			{Name: "two", ID: 2},
			{Name: "ten", ID: 10},
		},
	}
}
