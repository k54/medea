# Medea [![GoDoc][doc-img]][doc]

Medea is a fast and easy json serialization library


[doc-img]: https://godoc.org/gitlab.com/k54/medea?status.svg
[doc]: https://godoc.org/gitlab.com/k54/medea
