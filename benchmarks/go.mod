module benchmarks

go 1.14

replace gitlab.com/k54/medea => ../

require (
	github.com/francoispqt/gojay v1.2.13
	github.com/json-iterator/go v1.1.6
	gitlab.com/k54/medea v0.0.0-00010101000000-000000000000
)
