
test:
	go test

bench:
	go test -bench .

cov:
	go test -coverprofile cover.out
	grep -v -e " 1$$" cover.out
	$(RM) cover.out

size:
	rm -f *.gz
	rm -f *.lzma
	go test
	time lzma data.json
	time lzma data.medea
	go test
	time gzip data.json
	time gzip data.medea
	go test
