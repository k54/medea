package medea

import (
	"fmt"
	"strconv"
	"time"
)

func (je *jsonEncoder) Value(key string, v interface{}) {
	ok := valueOf(je, key, v)
	if !ok {
		je.String(key, fmt.Sprint(v))
	}
}

func (je *jsonEncoder) StringBytes(key string, value []byte) {
	je.writeKey(key)
	je.writeByte('"')
	je.buf = append(je.buf, value...)
	je.writeString(`",`)
}

func (je *jsonEncoder) RawString(key, value string) {
	je.writeKey(key)
	je.writeByte('"')
	je.writeString(value)
	je.writeString(`",`)
}

func (je *jsonEncoder) String(key, value string) {
	je.writeKey(key)
	je.writeByte('"')
	je.buf = writeEscapedString(je.buf, value)
	je.writeString(`",`)
}

func (je *jsonEncoder) Err(key string, value error) {
	je.String(key, value.Error())
}

func (je *jsonEncoder) Stringer(key string, value fmt.Stringer) {
	je.String(key, value.String())
}

func (je *jsonEncoder) Float(key string, value float64) {
	je.writeKey(key)
	je.buf = strconv.AppendFloat(je.buf, value, 'f', -1, 64)
	je.writeByte(',')
}

func (je *jsonEncoder) Nil(key string) {
	je.writeKey(key)
	je.writeString("null,")
}

func (je *jsonEncoder) Bool(key string, value bool) {
	je.writeKey(key)
	if value {
		je.writeString("true,")
	} else {
		je.writeString("false,")
	}
}

func (je *jsonEncoder) Key(key string) {
	je.writeKey(key)
}

func (je *jsonEncoder) ArrayFunc(key string, value func(Encoder)) {
	je.writeKey(key)
	je.writeByte('[')
	value(je)
	je.replaceLast(']')
	je.writeByte(',')
}

func (je *jsonEncoder) ObjectFunc(key string, value func(Encoder)) {
	je.writeKey(key)
	je.writeByte('{')
	value(je)
	je.replaceLast('}')
	je.writeByte(',')
}

func (je *jsonEncoder) Array(key string, value ArrayExporter) {
	if value.IsNil() {
		return
	}
	je.ArrayFunc(key, value.ExportArray)
}

func (je *jsonEncoder) Object(key string, value ObjectExporter) {
	if value.IsNil() {
		return
	}
	je.ObjectFunc(key, value.ExportObject)
}

func (je *jsonEncoder) Int(key string, value int) {
	je.Int64(key, int64(value))
}

func (je *jsonEncoder) Time(key string, value time.Time) {
	je.Int64(key, value.Unix())
}

func (je *jsonEncoder) Int64(key string, value int64) {
	je.writeKey(key)
	je.buf = appendInt(je.buf, value)
	je.writeByte(',')
}

func (je *jsonEncoder) Uint64(key string, value uint64) {
	je.writeKey(key)
	je.buf = appendUint(je.buf, value)
	je.writeByte(',')
}
